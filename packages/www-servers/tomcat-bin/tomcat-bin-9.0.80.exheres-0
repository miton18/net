# Copyright 2013-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service [ systemd_files=[ "${TEMP}"/systemd ] ]

MY_PN=${PN/-bin}
MY_PNV=${MY_PN}-${PV/-*}

SUMMARY="Implementation of the Java Servlet and JavaServer Pages technology"
HOMEPAGE="https://${MY_PN}.apache.org"
DOWNLOADS="mirror://apache/${MY_PN}/${MY_PN}-$(ever major)/v${PV/-*}/bin/apache-${MY_PNV}.tar.gz"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        group/tomcat
        user/tomcat
    run:
        virtual/jre:*[>=1.8]
    recommendation:
        www-servers/tomcat-native[>=2.0.5]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-Change-paths.patch
    "${FILES}"/0002-Change-conf-dir.patch
)

TOMCAT_DIR=/usr/share/${MY_PNV}
WORK=${WORKBASE}/apache-${MY_PNV}

pkg_setup() {
    default

    exdirectory --allow /srv
}

src_prepare() {
    default

    # Remove non-Linux stuff
    edo rm bin/*.bat

    edo mkdir "${TEMP}"/systemd
    edo cp "${FILES}"/systemd/tomcat.service "${TEMP}"/systemd
    edo cp "${FILES}"/systemd/tomcat.conf "${TEMP}"/systemd

    edo sed -i -e "s:TOMCAT_DIR:${TOMCAT_DIR}:" "${TEMP}"/systemd/tomcat.{service,conf}
}

src_install() {
    insinto "${TOMCAT_DIR}"
    doins -r bin
    doins -r lib

    insinto /etc/tomcat
    doins conf/*

    insinto /srv/tomcat
    doins -r webapps

    install_systemd_files
    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    newins "${FILES}"/systemd/tomcat-tmp.conf tomcat.conf
    insinto /etc/conf.d
    doins "${TEMP}"/systemd/tomcat.conf

    edo chmod +x "${IMAGE}"/"${TOMCAT_DIR}"/bin/*.sh

    keepdir \
            /var/cache/tomcat \
            /var/log/tomcat
    edo chown tomcat:tomcat \
            "${IMAGE}"/var/cache/tomcat \
            "${IMAGE}"/var/log/tomcat \
            "${IMAGE}"/srv/tomcat/webapps

    # These directories are hardcoded so let's symlink them into place
    dosym /etc/tomcat "${TOMCAT_DIR}"/conf
    dosym /var/cache/tomcat "${TOMCAT_DIR}"/work
}

