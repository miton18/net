# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require launchpad [ suffix=tgz ]
require python [ blacklist=3 multibuild=false ]

SUMMARY="The GNU mailing list manager"
SLOT="0"
LICENCES="GPL-2 GPL-3"

MYOPTIONS=""

PLATFORMS="~amd64"

DEPENDENCIES="
    build+run:
        user/apache
        user/mailman
    run:
        dev-python/dnspython[python_abis:*(-)?]
        virtual/mta
"

MAILMAN_INSTALL_DIR=/usr/$(exhost --target)/lib/${PN}

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --prefix=${MAILMAN_INSTALL_DIR}

    --with-python=/usr/$(exhost --target)/bin/python2
    --with-username=mailman
    --with-groupname=mailman
    --with-mail-gid=mailman
    --with-cgi-gid=apache
    --with-var-prefix=/srv/mailman

    # we don't chown until src_install
    --without-permcheck
)

src_install() {
    default
    edo chown -R mailman:mailman "${IMAGE}"/${MAILMAN_INSTALL_DIR}
    keepdir /usr/$(exhost --target)/lib/mailman/{logs,lists,locks,spam,qfiles,pythonlib}
    keepdir /usr/$(exhost --target)/lib/mailman/archives/{public,private}
    edo cd "${IMAGE}"
    edo mv {srv,usr/share/doc/${PNVR}/}
    find usr/share/doc/${PNVR}/srv/mailman -type d -empty | while read d; do
        edo keepdir /$d
    done
    edo chown -R apache:mailman "${IMAGE}"/usr/share/doc/${PNVR}/srv/mailman

    edo find "${IMAGE}"/${MAILMAN_INSTALL_DIR} -name *.py -exec \
        sed -i '1s:^#!.*python.*:#!/usr/bin/env python2:' {} \;
}

pkg_postinst() {
    default
    elog "Mailman has been installed into ${MAILMAN_INSTALL_DIR}."
    echo
    elog "It will look for var stuff in /srv/mailman. If that dir"
    elog "does not exist a populated copy can be taken from"
    elog "/usr/share/doc/${PNVR}/srv/mailman"
    echo
    elog "Mailman will expect to be running as the apache user."
    elog "If this isn't what you want, you should edit this exheres"
    elog "to use a different user, or install Mailman manually."
}

