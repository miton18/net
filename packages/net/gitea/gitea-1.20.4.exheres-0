# Copyright 2017-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=go-gitea release=v${PV} pnv=${PN}-src-${PV} suffix=tar.gz ] \
    systemd-service [ systemd_files=[ contrib/systemd/gitea.service ] ] \
    flag-o-matic

SUMMARY="Gitea - Git with a cup of tea"
HOMEPAGE+=" https://gitea.io"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# require additional go packages
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/go[>=1.20]
    build+run:
        user/gitea
        group/gitea
        sys-libs/pam
    run:
        dev-scm/git[>=2.0]
    suggestion:
        dev-db/sqlite:3 [[
            description = [ Required for using a plain SQLite database instead of MySQL or PostgreSQL ]
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.20.1-adjust-config.patch
    "${FILES}"/${PN}-1.20.1-adjust-service-file.patch
)

pkg_setup() {
    filter-ldflags -Wl,-O1 -Wl,--as-needed
}

src_prepare() {
    default

    export GOROOT="/usr/$(exhost --target)/lib/go"
    export GOPATH="${WORKBASE}"/build

    edo mkdir -p "${WORKBASE}"/build/src/code.gitea.io
    edo ln -s "${WORK}" "${WORKBASE}"/build/src/code.gitea.io/gitea
}

src_compile() {
    edo pushd "${WORKBASE}"/build/src/code.gitea.io/gitea

    edo go fix
    edo go build \
        -o bin/gitea \
        -tags='production sqlite pam cert' \
        -ldflags "${LDFLAGS} -X main.Version=${PV}"

    edo popd
}

src_install() {
    dobin "${WORKBASE}"/build/src/code.gitea.io/gitea/bin/gitea

    insinto /usr/share/gitea
    doins -r {public,templates}
    insinto /usr/share/gitea/options
    doins -r options/{gitignore,label,license,locale,readme}

    insinto /etc/gitea
    newins custom/conf/app.example.ini app.ini
    edo chown -R gitea:gitea "${IMAGE}"/etc/gitea
    edo chmod 0600 "${IMAGE}"/etc/gitea/app.ini

    keepdir /var/{lib,log}/gitea
    keepdir /var/lib/gitea/custom

    insinto /var/lib/gitea/conf
    doins -r options/locale

    edo chown gitea:gitea "${IMAGE}"/var/{lib,log}/gitea

    install_systemd_files
}

