# Copyright 2017-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service [ systemd_files=[ opt/teamviewer/tv_bin/script/teamviewerd.service ] ] \
    gtk-icon-cache

SUMMARY="Remote control and meeting solution"
DESCRIPTION="
TeamViewer provides easy, fast and secure remote access and meeting solutions to Linux, Windows
PCs, Apple PCs and various other platforms, including Android and iPhone. TeamViewer is free for
personal use. You can use TeamViewer completely free of charge to access your private computers or
to help your friends with their computer problems.
"
HOMEPAGE="https://www.teamviewer.com"
DOWNLOADS="
    listed-only:
        platform:amd64? ( https://dl.tvcdn.de/download/linux/version_$(ever major)x/${PN}_${PV}_amd64.deb )
"

LICENCES="
    AFL-2.1 [[ note = [ D-Bus component ] ]]
    BSD-3 [[ note = [ Google Coredumper component ] ]]
    BSD-4 [[ note = [ SSLeay component ] ]]
    TeamViewer
    openssl [[ note = [ OpenSSL component ] ]]
"
SLOT="0"
PLATFORMS="-* ~amd64"
MYOPTIONS="
    platform: amd64
"

RESTRICT="strip"

# bundles Qt/icu/libxcb-util in RTlib
DEPENDENCIES="
    run:
        dev-libs/expat
        dev-libs/glib:2
        dev-libs/libglvnd
        dev-libs/nspr
        dev-libs/nss
        media-libs/fontconfig
        media-libs/freetype:2
        sys-apps/dbus
        sys-libs/zlib
        x11-apps/xdg-utils
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/libXcomposite
        x11-libs/libXcursor
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libxkbcommon
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libXScrnSaver
        x11-libs/libXtst
        x11-utils/xcb-util
        x11-utils/xcb-util-image
        x11-utils/xcb-util-keysyms
        x11-utils/xcb-util-renderutil
        x11-utils/xcb-util-wm
    suggestion:
        fonts/liberation-fonts [[
            description = [ Recommended font ]
        ]]
        sys-apps/xdg-desktop-portal[screencast] [[
            description = [ Required on host system to allow screensharing within wayland sessions ]
        ]]
        sys-auth/polkit:1 [[
            description = [ Required to allow executing scripts as privileged user ]
        ]]
"

WORK=${WORKBASE}

pkg_setup() {
    exdirectory --allow /opt
}

src_unpack() {
    default
    edo tar xf data.tar.xz
}

src_prepare() {
    edo sed \
        -e 's:"$(dirname "$(readlink -e "$0")")":/opt/teamviewer/tv_bin/script:g' \
        -i opt/teamviewer/tv_bin/script/teamviewer
    edo sed \
        -e 's:/var/run:/run:g' \
        -i opt/teamviewer/tv_bin/script/{teamviewerd.service,tvw_config}
}

src_test() {
    :
}

src_install() {
    doins -r opt

    insinto /usr
    doins -r usr/share

    exeinto /usr/$(exhost --target)/bin
    doexe opt/teamviewer/tv_bin/script/${PN}

    keepdir /etc/${PN}
    keepdir /var/log/${PN}$(ever major)

    install_systemd_files

    edo chmod 0755 "${IMAGE}"/opt/teamviewer/tv_bin/{TeamViewer,teamviewer-config,teamviewer-managedevice,teamviewerd,TeamViewer_Desktop}
    edo chmod 0755 "${IMAGE}"/opt/teamviewer/tv_bin/script/{execscript,${PN}}

    edo rm "${IMAGE}"/opt/teamviewer/tv_bin/script/{teamviewerd.sysv,teamviewer_setup,tv-delayed-start.sh}
    edo rm -r "${IMAGE}"/opt/teamviewer/tv_bin/RTlib/qt/translations
    edo rm -r "${IMAGE}"/opt/teamviewer/tv_bin/xdg-utils
}

