# Copyright 2011 Ivan Dives
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.gz ] option-renames [ renames=[ 'gtk2 providers:gtk2' ] ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="w3m is a pager with WWW capability. It IS a pager, but it can be used as a text-mode WWW browser."

LICENCES="w3m"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gpm
    (
        imlib2 [[ description = [ Enable inline image rendering via imlib2. This supports png/jpeg/gif images (see imlib2's options) but doesn't support gif animation but has a very small dependency bloat. ] ]]
        gtk2 [[ description = [ Enable inline image rendering via gtk2. In addition to what imlib2 provides this enables gif animation but requires gtk+-2 to be installed. ] ]]
    ) [[ number-selected = at-most-one ]]
    X [[ description = [ Whether to enable or disable inline image rendering in X terminal emulators (xterm/rxvt/etc). If enabled requires X11 libs to be installed. (Does not matter if you didn't enable inline image rendering.) ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
    ( providers: gtk2 )
"

DEPENDENCIES="
    build:
        sys-devel/gettext
    build+run:
        dev-libs/boehm-gc[>=7.2]
        gpm? ( sys-libs/gpm )
        imlib2? ( media-libs/imlib2[X?] )
        providers:gtk2? ( x11-libs/gtk+:2 )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/040_link_gcc45.patch
    "${FILES}"/w3m-0.5.2-glibc2.14-fix_file_handle_error.patch
    -p0 "${FILES}"/build-against-gc7.2.patch
)

DEFAULT_SRC_COMPILE_PARAMS=( AR=${AR} )

src_prepare() {
    default
    eautoconf
}

src_configure() {
    # Here might appear a weird problem, sometimes with gtk2/fb linking fails. Couldn't hunt it down yet, if you encounter it, please report.
    # --with-imagelib=gdk-pixbuf doesn't work
    local inline_images

    if option imlib2 || option providers:gtk2; then
        inline_images="--enable-image=$(option X && echo x11,fb || echo fb) --with-imagelib=$(optionv imlib2)$(optionv providers:gtk2)"
    else
        inline_images=--enable-image=no
    fi

    econf \
        --without-migemo \
        --with-ssl \
        --with-termlib=ncurses \
        $(option gpm || echo --disable-mouse) \
        "${inline_images}"

    # the option is there but not exposed via configure
    edo cat >> config.h <<EOF
    #undef USE_EGD
EOF
}

src_install() {
    default

    insinto /usr/share/${PN}/Bonus
    doins Bonus/*
}

pkg_postinst() {
    default

    if option providers:gtk2; then
        elog "Notice"
        elog "If you want to see GIF animation, please hit a suitable key, such"
        elog "as 'h', 'l', etc., repeatedly, because a frame is rewritten"
        elog "according to the re-drawing demand from w3m."
    fi
}
