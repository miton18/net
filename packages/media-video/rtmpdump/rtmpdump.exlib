# Copyright 2010-2014 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SCM_REPOSITORY="https://git.ffmpeg.org/rtmpdump.git"

require scm-git option-renames [ renames=[ 'gnutls providers:gnutls' ] ]

export_exlib_phases src_prepare src_compile

SUMMARY="A toolkit for RTMP streams"
HOMEPAGE="http://rtmpdump.mplayerhq.hu/"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        sys-libs/zlib
        providers:gnutls? (
            dev-libs/gmp:=
            dev-libs/gnutls
            dev-libs/nettle:=
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
"



DEFAULT_SRC_INSTALL_PARAMS=(
    prefix=/usr/$(exhost --target)
    mandir=/usr/share/man
    sbindir=/usr/$(exhost --target)/bin
)

rtmpdump_src_prepare() {
    default

    has_version "dev-libs/openssl[>=1.1.0]" && expatch "${FILES}"/${PN}-openssl-1.1.patch
}

rtmpdump_src_compile() {
    if option providers:gnutls; then
        CRYPTO=GNUTLS
    else
        CRYPTO=OPENSSL
    fi

    emake AR="${AR}" CC="${CC}" CXX="${CXX}" XLDFLAGS="${LDFLAGS}" CRYPTO=${CRYPTO}
}

