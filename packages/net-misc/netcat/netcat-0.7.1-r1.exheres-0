# Copyright 2010 Michael Thomas <aelmalinka@gmail.com>
# Distributed under the terms of the GNU General Public Licesnse v2

require alternatives

SUMMARY="Netcat rewrite which aims to be fully compatible with the original nc 1.10"
DESCRIPTION="Netcat is a featured networking utility which reads and writes data across network connections, using the TCP/IP protocol.
It is designed to be a reliable 'back-end' tool that can be used directly or easily driven by other programs and scripts. At the same time, it is a feature-rich network debugging and exploration tool, since it can create almost any kind of connection you would need and has several interesting built-in capabilities."
DOWNLOADS="mirror://sourceforge/project/netcat/netcat/${PV}/${PNV}.tar.bz2"
HOMEPAGE="http://netcat.sourceforge.net/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="debug"

DEPENDENCIES="
    build:
        dev-lang/perl:*
    run:
        (
            !net-misc/netcat6[<1.0-r3]
            !net/netcat-openbsd[<1.105-r2]
        ) [[
            *description = [ Both install /usr/bin/nc and man pages ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-compat
    --disable-oldhexdump
    --disable-oldtelnet
    --hates=docdir
    --hates=datarootdir
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( debug )

src_install() {
    default

    # We'll manage nc symlink with alternatives
    edo rm "${IMAGE}/usr/$(exhost --target)/bin/nc"

    alternatives_for netcat gnu 1000 \
        /usr/$(exhost --target)/bin/nc netcat \
        /usr/share/man/man1/nc.1 netcat.1
}
