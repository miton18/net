# Copyright 2008 Santiago M. Mola
# Copyright 2008-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require debian-upstream [ suffix=tar.xz ]

export_exlib_phases src_prepare src_compile src_install

SUMMARY="Marco d'Itri's improved whois client"
DESCRIPTION="
This whois implementation supports all protocol extensions (RIPE, 6bone, CRSNIC)
with the familiar RIPE command line interface and IPv6 support. It also
automagically queries the right registry for all domains and most netblocks.
"
HOMEPAGE="https://www.linux.it/~md/software/"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    ( linguas: cs da de el es eu fi fr it ja pl pt_BR ru tr zh_CN )
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        virtual/pkg-config
    build+run:
        net-dns/libidn2:=[>=2.0.3]
"

WORK=${WORKBASE}/${PN}

print_catalogs() {
    local CATALOGS=""
    for i in ${LINGUAS}; do
        CATALOGS+=" ${i/%/.mo}"
    done
    echo ${CATALOGS}
}

DEFAULT_SRC_COMPILE_PARAMS=(
    CONFIG_FILE=/etc/whois.conf
    HAVE_ICONV=1
    OPTS="${CFLAGS}"
    CC="${CC}"
    CATALOGS="$(print_catalogs)"
    all
    pos
)

DEFAULT_SRC_INSTALL_PARAMS=(
    BASEDIR="${IMAGE}"
    CATALOGS="$(print_catalogs)"
    install-pos
)

whois_src_prepare() {
    # Keep mkpasswd out of the targets all and install
    edo sed \
        -e '/^all:/s:mkpasswd::' \
        -e '/^install:/s:install-mkpasswd::' \
        -i Makefile

    default
}

whois_src_compile() {
    emake servers_charset.h version.h
    default
}

whois_src_install() {
    default

    edo mkdir -p "${IMAGE}"/usr/$(exhost --target)
    edo mv "${IMAGE}"/usr/{bin,$(exhost --target)/}

    insinto /etc
    doins whois.conf
}

